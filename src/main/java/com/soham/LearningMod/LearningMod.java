package com.soham.LearningMod;

import com.soham.LearningMod.Tab.CreativeTab;
import com.soham.LearningMod.blocks.blocks;
import com.soham.LearningMod.gui.GUI;
import com.soham.LearningMod.items.items;
import com.soham.LearningMod.proxies.CommonProxy;
import com.soham.LearningMod.worldgen.worldgen;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;


@Mod(modid = Reference.MODID, name = Reference.MODN, version = Reference.MODV)
public class LearningMod {
	
	@Instance(Reference.MODID)
	public static LearningMod instance;
	
	@SidedProxy(clientSide = "com.soham.LearningMod.proxies.ClientProxy", serverSide = "com.soham.LearningMod.proxies.CommonProxy")
	public static CommonProxy proxy;
	
	@EventHandler
	public static void preinit(FMLPreInitializationEvent event){
		CreativeTab.init();
		blocks.init();
		items.init();
		worldgen.init();
		GUI.init();
	}
	
	@EventHandler
	public static void init(FMLInitializationEvent event){
		
		
	}
	
	@EventHandler
	public static void postinit(FMLPostInitializationEvent event){
		
		
	}
	
	
	
	
	
	

}
