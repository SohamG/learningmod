package com.soham.LearningMod.worldgen;

import java.util.Random;

import com.soham.LearningMod.blocks.blocks;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import cpw.mods.fml.common.IWorldGenerator;

public class Oregenerator implements IWorldGenerator {

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {

		switch(world.provider.dimensionId)
		{
		case 0: generateSurface(random, chunkX*16, chunkZ*16, world); break;
		case 1: generateEnd(random, chunkX*16, chunkZ*16, world); break;
		case -1: generateNether(random, chunkX*16, chunkZ*16, world); break;
		default:;
		}
	}

	private void generateNether(Random random, int i, int j, World world) {
		addOreSpwan(blocks.chromiumOre, world, random, i, j, 3, 8, 100, 3, 65, Blocks.netherrack);

	}

	private void generateEnd(Random random, int i, int j, World world) {
		// TODO Auto-generated method stub

	}

	public void generateSurface(Random random, int i, int j, World world) {
		addOreSpwan(blocks.chromiumOre, world, random, i, j, 3, 8, 100, 3, 65, Blocks.stone);
		

	}
	
	private static void addOreSpwan(Block block, World world, Random random, int blockXPos, int blockZPos, int minVainSize, int maxVainSize, int chancesToSpawn, int minY, int maxY , Block toReplace){
		for(int i = 0; i<chancesToSpawn; i++)
        {
            int posX = blockXPos + random.nextInt(16);
            int posY = minY + random.nextInt(maxY - minY);
            int posZ = blockZPos + random.nextInt(16);
            new WorldGenMinable(block, (minVainSize + random.nextInt(maxVainSize - minVainSize)), toReplace).generate(world, random, posX, posY, posZ);
        }
	}

}


