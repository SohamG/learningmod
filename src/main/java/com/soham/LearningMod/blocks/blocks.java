package com.soham.LearningMod.blocks;

import net.minecraft.block.Block;
import cpw.mods.fml.common.registry.GameRegistry;
import com.soham.LearningMod.blocks.BlockChromiumFurnace.*;

public class blocks {
	
	public static Block chromiumFurnace;
	//public static Block chromiumFurnaceActive;
	
	public static Block chromiumOre;
	
	public static void init(){
		
		chromiumFurnace = new BlockChromiumFurnace().setBlockName(BlockInfo.FURNACE_UNLOCAL_IDLE);
		//chromiumFurnaceActive = new BlockChromiumFurnace(true).setBlockName(BlockInfo.FURNACE_UNLOCAL_IDLE).setLightLevel(0.9F);
		
		chromiumOre = new BlockChromiumOre();
		
		
		registerBlocks();
	}

	private static void registerBlocks() {
		GameRegistry.registerBlock(chromiumFurnace, chromiumFurnace.getUnlocalizedName().substring(5));
		GameRegistry.registerBlock(chromiumOre, chromiumOre.getUnlocalizedName().substring(5));

	}

}
