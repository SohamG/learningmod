package com.soham.LearningMod.blocks;

import com.soham.LearningMod.Reference;
import com.soham.LearningMod.Tab.CreativeTab;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;

public class BlockChromiumOre extends Block {

	public BlockChromiumOre() {
		super(Material.rock);
		setHarvestLevel("pickaxe", 2);
		setBlockName(BlockInfo.ORE_UNLOCAL);
		setCreativeTab(CreativeTab.tablearning);
	}
	
	
	@Override
	public void registerBlockIcons(IIconRegister register){
		blockIcon = register.registerIcon(Reference.MODID + ":" + this.getUnlocalizedName().substring(5));
				
		
	}

}
