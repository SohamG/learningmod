package com.soham.LearningMod.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

import com.soham.LearningMod.LearningMod;
import com.soham.LearningMod.Reference;
import com.soham.LearningMod.Tab.CreativeTab;
import com.soham.LearningMod.tileentities.TileChromiumFurnace;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockChromiumFurnace extends BlockContainer {

	public BlockChromiumFurnace() {
		super(Material.iron);
		setCreativeTab(CreativeTab.tablearning);

	}

	@SideOnly(Side.CLIENT)
	private IIcon front;

	@SideOnly(Side.CLIENT)
	private IIcon side;

	private final Random field_149933_a = new Random();
	private final boolean field_149932_b = false;
	private static boolean field_149934_M;
	@SideOnly(Side.CLIENT)
	private IIcon field_149935_N;
	@SideOnly(Side.CLIENT)
	private IIcon field_149936_O;

	public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_) {
		return Item.getItemFromBlock(Blocks.furnace);
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	public void onBlockAdded(World p_149726_1_, int p_149726_2_,
			int p_149726_3_, int p_149726_4_) {
		super.onBlockAdded(p_149726_1_, p_149726_2_, p_149726_3_, p_149726_4_);
		this.func_149930_e(p_149726_1_, p_149726_2_, p_149726_3_, p_149726_4_);
	}

	private void func_149930_e(World p_149930_1_, int p_149930_2_,int p_149930_3_, int p_149930_4_) {
		if (!p_149930_1_.isRemote) {
			Block block = p_149930_1_.getBlock(p_149930_2_, p_149930_3_, p_149930_4_ - 1);
			Block block1 = p_149930_1_.getBlock(p_149930_2_, p_149930_3_, p_149930_4_ + 1);
			Block block2 = p_149930_1_.getBlock(p_149930_2_ - 1, p_149930_3_, p_149930_4_);
			Block block3 = p_149930_1_.getBlock(p_149930_2_ + 1, p_149930_3_, p_149930_4_);
			byte b0 = 3;

			if (block.func_149730_j() && !block1.func_149730_j()) {
				b0 = 3;
			}

			if (block1.func_149730_j() && !block.func_149730_j()) {
				b0 = 2;
			}

			if (block2.func_149730_j() && !block3.func_149730_j()) {
				b0 = 5;
			}

			if (block3.func_149730_j() && !block2.func_149730_j()) {
				b0 = 4;
			}

			p_149930_1_.setBlockMetadataWithNotify(p_149930_2_, p_149930_3_,
					p_149930_4_, b0, 2);
		}
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
		if(!world.isRemote){
			player.openGui(LearningMod.instance, BlockInfo.FURNACE_GUI_ID, world, x, y, z);

		}

		return true;
	}

	

	/**
	 * Called when the block is placed in the world.
	 */
	public void onBlockPlacedBy(World p_149689_1_, int p_149689_2_,
			int p_149689_3_, int p_149689_4_, EntityLivingBase p_149689_5_,
			ItemStack p_149689_6_) {
		int l = MathHelper
				.floor_double((double) (p_149689_5_.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;

		if (l == 0) {
			p_149689_1_.setBlockMetadataWithNotify(p_149689_2_, p_149689_3_,
					p_149689_4_, 2, 2);
		}

		if (l == 1) {
			p_149689_1_.setBlockMetadataWithNotify(p_149689_2_, p_149689_3_,
					p_149689_4_, 5, 2);
		}

		if (l == 2) {
			p_149689_1_.setBlockMetadataWithNotify(p_149689_2_, p_149689_3_,
					p_149689_4_, 3, 2);
		}

		if (l == 3) {
			p_149689_1_.setBlockMetadataWithNotify(p_149689_2_, p_149689_3_,
					p_149689_4_, 4, 2);
		}

		if (p_149689_6_.hasDisplayName()) {
			((TileEntityFurnace) p_149689_1_.getTileEntity(p_149689_2_,
					p_149689_3_, p_149689_4_)).func_145951_a(p_149689_6_
					.getDisplayName());
		}
	}

	

	/**
	 * A randomly called display update to be able to add particles or other
	 * items for display
	 */
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(World p_149734_1_, int p_149734_2_,
			int p_149734_3_, int p_149734_4_, Random p_149734_5_) {
		if (this.field_149932_b) {
			int l = p_149734_1_.getBlockMetadata(p_149734_2_, p_149734_3_,
					p_149734_4_);
			float f = (float) p_149734_2_ + 0.5F;
			float f1 = (float) p_149734_3_ + 0.0F + p_149734_5_.nextFloat()
					* 6.0F / 16.0F;
			float f2 = (float) p_149734_4_ + 0.5F;
			float f3 = 0.52F;
			float f4 = p_149734_5_.nextFloat() * 0.6F - 0.3F;

			if (l == 4) {
				p_149734_1_.spawnParticle("smoke", (double) (f - f3),
						(double) f1, (double) (f2 + f4), 0.0D, 0.0D, 0.0D);
				p_149734_1_.spawnParticle("flame", (double) (f - f3),
						(double) f1, (double) (f2 + f4), 0.0D, 0.0D, 0.0D);
			} else if (l == 5) {
				p_149734_1_.spawnParticle("smoke", (double) (f + f3),
						(double) f1, (double) (f2 + f4), 0.0D, 0.0D, 0.0D);
				p_149734_1_.spawnParticle("flame", (double) (f + f3),
						(double) f1, (double) (f2 + f4), 0.0D, 0.0D, 0.0D);
			} else if (l == 2) {
				p_149734_1_.spawnParticle("smoke", (double) (f + f4),
						(double) f1, (double) (f2 - f3), 0.0D, 0.0D, 0.0D);
				p_149734_1_.spawnParticle("flame", (double) (f + f4),
						(double) f1, (double) (f2 - f3), 0.0D, 0.0D, 0.0D);
			} else if (l == 3) {
				p_149734_1_.spawnParticle("smoke", (double) (f + f4),
						(double) f1, (double) (f2 + f3), 0.0D, 0.0D, 0.0D);
				p_149734_1_.spawnParticle("flame", (double) (f + f4),
						(double) f1, (double) (f2 + f3), 0.0D, 0.0D, 0.0D);
			}
		}
	}

	/**
	 * If this returns true, then comparators facing away from this block will
	 * use the value from getComparatorInputOverride instead of the actual
	 * redstone signal strength.
	 */
	public boolean hasComparatorInputOverride() {
		return true;
	}

	/**
	 * If hasComparatorInputOverride returns true, the return value from this is
	 * used instead of the redstone signal strength when this block inputs to a
	 * comparator.
	 */
	public int getComparatorInputOverride(World p_149736_1_, int p_149736_2_,
			int p_149736_3_, int p_149736_4_, int p_149736_5_) {
		return Container.calcRedstoneFromInventory((IInventory) p_149736_1_
				.getTileEntity(p_149736_2_, p_149736_3_, p_149736_4_));
	}

	/**
	 * Gets an item for the block being called on. Args: world, x, y, z
	 */
	@SideOnly(Side.CLIENT)
	public Item getItem(World p_149694_1_, int p_149694_2_, int p_149694_3_,
			int p_149694_4_) {
		return Item.getItemFromBlock(Blocks.furnace);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister register) {

		front = register.registerIcon(Reference.MODID + ":"
				+ BlockInfo.FURNACE_TEXTURE_FRONT);
		side = register.registerIcon(Reference.MODID + ":"
				+ BlockInfo.FURNACE_TEXTURE_SIDE);

	}

	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int p_149691_1_, int p_149691_2_) {
		return p_149691_1_ == 1 ? this.side : (p_149691_1_ == 0 ? this.side
				: (p_149691_1_ != p_149691_2_ ? this.side : this.front));
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TileChromiumFurnace();
	}

}
