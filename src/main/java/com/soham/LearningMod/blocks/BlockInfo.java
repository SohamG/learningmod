package com.soham.LearningMod.blocks;

public class BlockInfo {
	
	public static final String FURNACE_UNLOCAL_IDLE = "chromiumFurnaceIdle";
	public static final String FURNACE_UNLOCAL_ACTIVE = "chromiumFurnaceActive";
	public static final String FURNACE_TEXTURE_SIDE = "furnace_side";
	public static final String FURNACE_TEXTURE_FRONT = "furnace_inactive";
	public static final String ORE_UNLOCAL = "chromiumore";
	public static final int FURNACE_GUI_ID = 1;
		
}
