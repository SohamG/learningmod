package com.soham.LearningMod.items;

import com.soham.LearningMod.Tab.CreativeTab;
import com.soham.LearningMod.Tab.Tab;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemWorkBench extends Item {
	
	public ItemWorkBench(){
		super();
		setUnlocalizedName("portabench");
		setCreativeTab(CreativeTab.tablearning);
		}
	
	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World world, EntityPlayer player)
    {
		if(world.isRemote)
		player.displayGUIWorkbench((int)player.posX, (int)player.posY, (int)player.posZ);
		
        return par1ItemStack;
    }
	
	

	
	
	
	
	
	
	
	
	
}
