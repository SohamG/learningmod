package com.soham.LearningMod.gui.ui;

import com.soham.LearningMod.Reference;
import com.soham.LearningMod.gui.container.FurnaceContainer;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class FurnaceGUI extends GuiContainer {
	
	private static final ResourceLocation location = new ResourceLocation(Reference.MODID.toLowerCase() + ":" + "textures/gui/chrom_furnace.png");
	
	int xSize = 176;
	int ySize = 165;
	public FurnaceGUI(InventoryPlayer inventory, TileEntity tile) {
		super(new FurnaceContainer(inventory, tile));
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
		
	}
	@Override
    public void drawScreen(int par1, int par2, float par3)
    {
        //Bind Texture
        this.mc.getTextureManager().bindTexture(location);
        // set the x for the texture, Total width - textureSize / 2
        par2 = (this.width - xSize) / 2;
        // set the y for the texture, Total height - textureSize - 30 (up) / 2,
        int j = (this.height - ySize - 30) / 2;
        // draw the texture
        drawTexturedModalRect(par2, j, 0, 0, xSize,  ySize);
    }
	
	@Override
	public boolean doesGuiPauseGame(){
		return false;
	}

}
