package com.soham.LearningMod.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.soham.LearningMod.gui.container.FurnaceContainer;
import com.soham.LearningMod.gui.ui.FurnaceGUI;
import com.soham.LearningMod.tileentities.TileChromiumFurnace;

import cpw.mods.fml.common.network.IGuiHandler;

public class GUIHandler implements IGuiHandler {

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tile = world.getTileEntity(x, y, z);

		switch (ID) {
		case 1:
			if (tile instanceof TileChromiumFurnace)
				return new FurnaceContainer(player.inventory, tile);

		}
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tile = world.getTileEntity(x, y, z);
		
		switch(ID){
			case 1: if(tile instanceof TileChromiumFurnace)return new FurnaceGUI(player.inventory, tile);
					break;
		}
		
		return null;
	}

}
